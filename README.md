# Product
Allows users to create, edit, delete and search for their products. Integrate with inventory( and store) service.

# Development Environment
To properly use product service requires a mongodb database with index explicitly created beforehand.
`cmd/local/local.go` contains the file for testing locally without the auth middleware.
`cmd/main/main.go` contains the actual application that has auth middleware - it requires env to be set prior
in addition to having a mongodb instance. Dockercompose file shows the example of the env that has to be set.

## Docker Compose for testing
Change directory into the root of the project and run the command `docker-compose up` to start mongodb and the product service. 

Then use MongoDB compass application or mongoshell to create a new database `compose` and collection `product`. Then create 2 unique compound index:
- For the first compound index use `userId` and `name` as key, also use the unique option.
- For the second compound index use `userId` and `_id` as key, also use the unique option.
 
To stop you can use `docker-compose stop` or `docker-compose down`. The latter option deletes the container as well. Since we bind mount mongodb, it is not neccessary to create index the next time we start up, unless you manually deleted them.
