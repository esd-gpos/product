module gitlab.com/esd-gpos/product

go 1.13

require (
	github.com/elastic/go-elasticsearch/v7 v7.6.0
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.1
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/google/uuid v1.1.1
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/esd-gpos/auth v0.0.0-20200330100020-24e8e06e3103
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200321134203-328b4cd54aae // indirect
)
