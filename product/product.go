package product

// Package for Product HTTP endpoint
// UserId format (uuid4) should be checked through Auth Middleware prior

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"gitlab.com/esd-gpos/auth/middleware"
	"math"
	"net/http"
	"strings"
)

func Setup(engine *gin.Engine, auth *middleware.Middleware, handler *Handler) {
	setValidation()
	setRoutes(engine, auth, handler)
}

func setValidation() {
	validate := binding.Validator.Engine().(*validator.Validate)
	_ = validate.RegisterValidation(prod_name, NameValidation)
	_ = validate.RegisterValidation(prod_tags, TagsValidation)
	_ = validate.RegisterValidation(prod_price, PriceValidation)
}

func setRoutes(engine *gin.Engine, auth *middleware.Middleware, handler *Handler) {
	group := engine.Group("api/v1/product")
	group.Use(auth.GinHandler)
	group.POST("", handler.AddProduct)
	group.GET("", handler.ListProduct)
	group.GET(":productId", handler.GetProduct)
	group.PUT(":productId", handler.UpdateProduct)
	group.DELETE(":productId", handler.DeleteProduct)
}

type mapping map[string]interface{}

// Entity is the complete model of product type
// It is to be used with our data sources through Repo interface
type Entity struct {
	UserId    string   `json:"-"`
	ProductId string   `json:"product_id"`
	Name      string   `json:"name"`
	Tags      []string `json:"tags"`
	Price     float64  `json:"price"`
	Desc      string   `json:"desc"`
}

// AddProduct struct is to be bind with AddProduct handler request body
type AddProduct struct {
	Name  string   `json:"name" binding:"required,prod_name"`
	Tags  []string `json:"tags" binding:"unique,prod_tags"`
	Price float64  `json:"price" binding:"prod_price"`
	Desc  string   `json:"desc"`
}

type UpdateProduct AddProduct

// Field level validation for each field in our struct
// Tag names for each FieldLevel Validation function
const (
	prod_name  = "prod_name"
	prod_tags  = "prod_tags"
	prod_price = "prod_price"
)

// Name Validation for our struct
func NameValidation(fl validator.FieldLevel) bool {
	if name := fl.Field().String(); strings.TrimSpace(name) == "" {
		return false
	}

	return true
}

// Tags Validation for our struct
func TagsValidation(fl validator.FieldLevel) bool {
	tags := fl.Field().Interface().([]string)
	for _, t := range tags {
		if strings.TrimSpace(t) == "" {
			return false
		}
	}

	return true
}

// Price Validation for our struct
func PriceValidation(fl validator.FieldLevel) bool {
	if price := fl.Field().Float(); price < 0 {
		return false
	}

	return true
}

// Repo interface defines data to be passed to data sources
// The followings are errors that Repo interface should use
// Unknown errors will be considered as internal error
var (
	errNotFound  = errors.New("product not found")
	errDuplicate = errors.New("product already exist")
)

// Repo interface defines which method must be supported to interact with data source
type Repo interface {
	// AddProduct accepts an Entity that will be used to create a new Product
	// Error occurs if Product with the same Name or productId already exist
	AddProduct(entity *Entity, ctx context.Context) error

	// ListProduct uses UserId to list out the Product that user have
	// Returns a slice of Entity objects
	ListProduct(userId string, ctx context.Context) ([]Entity, error)

	// GetProduct accepts an Entity, it uses UserId and ProductId to find the Product
	// It populates the entity before return
	// Error occurs if Product does not exist
	GetProduct(entity *Entity, ctx context.Context) error

	// UpdateProduct accepts an Entity that will be used to replace its old values
	// Error occurs if Product does not exist
	UpdateProduct(entity *Entity, ctx context.Context) error

	// DeleteProduct accepts UserId and ProductId
	// Error occurs if Product does not exist
	DeleteProduct(userId string, productId string, ctx context.Context) error
}

// Handler is a struct that will contain Repo to interact with data source
// Below are responses for the handler
const (
	// success response message for each handler
	resAddProduct    = "product added"
	resListProduct   = "list of products retrieved"
	resGetProduct    = "product retrieved"
	resUpdateProduct = "product updated"
	resDeleteProduct = "product deleted"

	// error response messages
	resInternal        = "internal error"
	resInvalid         = "invalid format"
	resDuplicate       = "product already exist"
	resProductNotFound = "product not found"
)

// Each handler methods corresponds to a HTTP endpoint
type Handler struct {
	repo Repo
}

func NewHandler(repo Repo) *Handler {
	return &Handler{repo: repo}
}

func (h *Handler) AddProduct(ctx *gin.Context) {
	// Get UserId through context
	userId, err := getUserId(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	// Bind request body to AddProduct object
	body := AddProduct{}
	if err := ctx.ShouldBindJSON(&body); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	// Add and validate the Entity object
	entity := Entity{
		UserId:    userId,
		ProductId: uuid.New().String(),
		Name:      body.Name,
		Tags:      body.Tags,
		Price:     math.Round((body.Price * 100) / 100),
		Desc:      body.Desc,
	}

	// Use Repo to interact with data source
	if err := h.repo.AddProduct(&entity, ctx); err != nil {
		if err == errDuplicate {
			ctx.JSON(http.StatusConflict, gin.H{"message": resDuplicate})
			return
		}

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": resAddProduct, "result": entity})
}

func (h *Handler) ListProduct(ctx *gin.Context) {
	// Get UserId through context
	userId, err := getUserId(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	// Use Repo to list products
	entities, err := h.repo.ListProduct(userId, ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resListProduct, "result": entities})
}

func (h *Handler) GetProduct(ctx *gin.Context) {
	// Get UserId through context
	userId, err := getUserId(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	// Get ProductId through params
	// Create Entity object and validate
	productId := ctx.Param("productId")
	entity := Entity{UserId: userId, ProductId: productId}
	if _, err := uuid.Parse(productId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	// Use Repo to get Product
	if err := h.repo.GetProduct(&entity, ctx); err != nil {
		if err == errNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resProductNotFound})
			return
		}

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resGetProduct, "result": entity})
}

func (h *Handler) UpdateProduct(ctx *gin.Context) {
	// Get UserId through context
	userId, err := getUserId(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	// Get ProductId through params
	productId := ctx.Param("productId")
	if _, err := uuid.Parse(productId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	// Bind request to UpdateProduct object
	updateProduct := UpdateProduct{}
	if err := ctx.ShouldBindJSON(&updateProduct); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	// Create and validate Entity object
	entity := Entity{
		UserId:    userId,
		ProductId: productId,
		Name:      updateProduct.Name,
		Tags:      updateProduct.Tags,
		Price:     updateProduct.Price,
		Desc:      updateProduct.Desc,
	}

	// Use Repo to update Product
	if err := h.repo.UpdateProduct(&entity, ctx); err != nil {
		if err == errNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resProductNotFound})
			return
		}

		if err == errDuplicate {
			ctx.JSON(http.StatusConflict, gin.H{"message": resDuplicate})
			return
		}

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resUpdateProduct, "result": entity})
}

func (h *Handler) DeleteProduct(ctx *gin.Context) {
	// Get UserId through context
	userId, err := getUserId(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	// Get and validate ProductId through params
	productId := ctx.Param("productId")
	if _, err := uuid.Parse(productId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	// Use Repo to delete Product
	if err := h.repo.DeleteProduct(userId, productId, ctx); err != nil {
		if err == errNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resProductNotFound})
			return
		}

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resDeleteProduct})
}

// Utility function
func getUserId(ctx *gin.Context) (string, error) {
	value, exist := ctx.Get("userId")
	if !exist {
		return "", errors.New("cannot retrieve userId")
	}

	if userId, ok := value.(string); ok {
		return userId, nil
	}

	return "", errors.New("cannot retrieve userId")
}
