package product

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// To use MongoRepo struct, MongoDB collection should have 2 index created
// The default _id field and compound index on userId and name field

// MongoRepo struct implements Repo interface
// Will be used in Handler struct
type MongoRepo struct {
	c *mongo.Collection
}

func NewMongoRepo(c *mongo.Collection) *MongoRepo {
	return &MongoRepo{c: c}
}

func (m *MongoRepo) AddProduct(entity *Entity, ctx context.Context) error {
	_, err := m.c.InsertOne(ctx, bson.M{
		"_id":    entity.ProductId,
		"userId": entity.UserId,
		"name":   entity.Name,
		"tags":   entity.Tags,
		"price":  entity.Price,
		"desc":   entity.Desc,
	})
	if err != nil {
		writeException, ok := err.(mongo.WriteException)
		if !ok {
			return err
		}

		writeError := writeException.WriteErrors

		// if not write error - some internal error
		if len(writeError) == 0 {
			return err
		}

		// if duplicate error
		if writeError[0].Code == 11000 {
			return errDuplicate
		}

		return err
	}

	return nil
}

func (m *MongoRepo) ListProduct(userId string, ctx context.Context) ([]Entity, error) {
	cur, err := m.c.Find(ctx, bson.M{"userId": userId})
	if err != nil {
		return nil, err
	}

	entities := make([]Entity, 0)
	index := 0

	for cur.Next(ctx) {
		result := mapping{}
		if err = cur.Decode(&result); err != nil {
			return nil, err
		}

		entities = append(entities, Entity{
			UserId:    userId,
			ProductId: result["_id"].(string),
			Name:      result["name"].(string),
			Tags:      make([]string, len(result["tags"].(primitive.A))),
			Price:     result["price"].(float64),
			Desc:      result["desc"].(string),
		})

		for i, t := range result["tags"].(primitive.A) {
			entities[index].Tags[i] = t.(string)
		}

		index++
	}

	return entities, nil
}

func (m *MongoRepo) GetProduct(entity *Entity, ctx context.Context) error {
	single := m.c.FindOne(ctx, bson.M{"userId": entity.UserId, "_id": entity.ProductId})
	if single.Err() == mongo.ErrNoDocuments {
		return errNotFound
	}

	result := mapping{}
	if err := single.Decode(&result); err != nil {
		return err
	}

	entity.Name = result["name"].(string)
	entity.Tags = make([]string, len(result["tags"].(primitive.A)))
	entity.Price = result["price"].(float64)
	entity.Desc = result["desc"].(string)

	for i, t := range result["tags"].(primitive.A) {
		entity.Tags[i] = t.(string)
	}

	return nil
}

func (m *MongoRepo) UpdateProduct(entity *Entity, ctx context.Context) error {
	// Check
	single := m.c.FindOneAndReplace(ctx, bson.M{"userId": entity.UserId, "_id": entity.ProductId}, bson.M{
		"userId": entity.UserId,
		"name":   entity.Name,
		"tags":   entity.Tags,
		"price":  entity.Price,
		"desc":   entity.Desc,
	})
	if single.Err() == mongo.ErrNoDocuments {
		return errNotFound
	}

	if err, ok := single.Err().(mongo.CommandError); ok {
		if err.Code == 11000 {
			return errDuplicate
		}

		return err
	}

	if single.Err() != nil {
		return single.Err()
	}

	return nil
}

func (m *MongoRepo) DeleteProduct(userId string, productId string, ctx context.Context) error {
	single := m.c.FindOneAndDelete(ctx, bson.M{"userId": userId, "_id": productId})
	if single.Err() != nil {
		if single.Err() == mongo.ErrNoDocuments {
			return errNotFound
		}

		return single.Err()
	}

	return nil
}
