package product

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

// Constants for testing
const (
	goodProductId     = "7bd6c3cb-34e9-4579-9fe6-168cc2653e09"
	missingProductId  = "b3699e06-ae60-4ff8-b182-406078e27bb8"
	internalProductId = "66c8800e-c2ca-4c8d-af10-207f4bf1cf26"
	userId            = "39dd16b7-042d-453a-89a6-c6e1ec3b44a7"
	goodName          = "GoodName"
	duplicateName     = "DuplicateName"
)

// Struct implementing Repo interface
// Used for testing
type repoTest struct{}

func (r *repoTest) AddProduct(entity *Entity, ctx context.Context) error {
	if entity.Name == goodName {
		return nil
	} else if entity.Name == duplicateName {
		return errDuplicate
	}

	return errors.New("internal error or something")
}

func (r *repoTest) ListProduct(userId string, ctx context.Context) ([]Entity, error) {
	return []Entity{}, nil
}

func (r *repoTest) GetProduct(entity *Entity, ctx context.Context) error {
	if entity.ProductId == goodProductId {
		return nil
	} else if entity.ProductId == missingProductId {
		return errNotFound
	}

	return errors.New("some internal error")
}

func (r *repoTest) UpdateProduct(entity *Entity, ctx context.Context) error {
	if entity.ProductId == missingProductId {
		return errNotFound
	} else if entity.Name == duplicateName {
		return errDuplicate
	} else if entity.Name == goodName && entity.ProductId == goodProductId {
		return nil
	}

	return errors.New("some internal error")
}

func (r *repoTest) DeleteProduct(userId string, productId string, ctx context.Context) error {
	if productId == goodProductId {
		return nil
	} else if productId == missingProductId {
		return errNotFound
	}

	return errors.New("some internal error")
}

// Setup handler for testing
var handler = &Handler{repo: &repoTest{}}

func setUp() *gin.Engine {
	gin.SetMode(gin.TestMode)

	engine := gin.New()
	validate := binding.Validator.Engine().(*validator.Validate)
	_ = validate.RegisterValidation(prod_name, NameValidation)
	_ = validate.RegisterValidation(prod_tags, TagsValidation)
	_ = validate.RegisterValidation(prod_price, PriceValidation)

	engine.Use(func(ctx *gin.Context) {
		ctx.Set("userId", userId)
	})

	return engine
}

// Start of tests
// FieldLevel validation function testing
func TestNameValidation(t *testing.T) {
	validate := validator.New()
	if err := validate.RegisterValidation(prod_name, NameValidation); err != nil {
		t.Fatal(err)
	}

	type input struct {
		Name string `validate:"required,prod_name"`
	}

	testCases := []struct {
		in  input
		out bool
	}{
		{in: input{Name: "Hello"}, out: true},
		{in: input{Name: "H"}, out: true},
		{in: input{"   fewff   "}, out: true},
		{in: input{"    "}, out: false},
	}

	for i, c := range testCases {
		if err := validate.Struct(c.in); (err == nil) != c.out {
			t.Fatalf("test case [%d]: %s", i, err)
		}
	}
}

func TestTagsValidation(t *testing.T) {
	validate := validator.New()
	if err := validate.RegisterValidation(prod_tags, TagsValidation); err != nil {
		t.Fatal(err)
	}

	type input struct {
		Tags []string `validate:"unique,prod_tags"`
	}

	testCases := []struct {
		in  input
		out bool
	}{
		{in: input{Tags: []string{"Hello"}}, out: true},
		{in: input{Tags: []string{}}, out: true},
		{in: input{Tags: []string{" "}}, out: false},
		{in: input{Tags: []string{"Hello", " "}}, out: false},
		{in: input{Tags: nil}, out: true},
		{in: input{Tags: []string{"H", "H"}}, out: false},
	}

	for i, c := range testCases {
		if err := validate.Struct(c.in); (err == nil) != c.out {
			t.Fatalf("test case [%d]: %s", i, err)
		}
	}
}

func TestPriceValidation(t *testing.T) {
	validate := validator.New()
	if err := validate.RegisterValidation(prod_price, PriceValidation); err != nil {
		t.Fatal(err)
	}

	type input struct {
		Price float32 `validate:"prod_price"`
	}

	testCases := []struct {
		in  input
		out bool
	}{
		{in: input{Price: 10}, out: true},
		{in: input{Price: 0}, out: true},
		{in: input{Price: -1}, out: false},
	}

	for i, c := range testCases {
		if err := validate.Struct(c.in); (err == nil) != c.out {
			t.Fatalf("test case [%d]: %s", i, err)
		}
	}
}

// Tests for handler
func TestHandler_AddProduct(t *testing.T) {
	engine := setUp()
	engine.POST("", handler.AddProduct)

	testCases := []struct {
		in      gin.H
		message string
		code    int
	}{
		{
			in:      gin.H{"name": goodName, "price": 10},
			message: resAddProduct,
			code:    http.StatusCreated,
		}, {
			in:      gin.H{"name": duplicateName, "price": 10},
			message: resDuplicate,
			code:    http.StatusConflict,
		}, {
			in:      gin.H{"name": "internal error", "tags": []string{}, "price": 10},
			message: resInternal,
			code:    http.StatusInternalServerError,
		}, {
			in:      gin.H{"name": goodName, "price": -1},
			message: resInvalid,
			code:    http.StatusBadRequest,
		},
	}

	for i, c := range testCases {
		resp := httptest.NewRecorder()

		body, _ := json.Marshal(c.in)
		req, _ := http.NewRequest(http.MethodPost, "/", bytes.NewReader(body))

		engine.ServeHTTP(resp, req)

		respBody := gin.H{}
		_ = json.Unmarshal(resp.Body.Bytes(), &respBody)

		if c.code != resp.Code {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.code, resp.Code)
		}

		if c.message != respBody["message"] {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.message, respBody["message"])
		}
	}
}

func TestHandler_ListProduct(t *testing.T) {
	// TODO
}

func TestHandler_GetProduct(t *testing.T) {
	engine := setUp()
	engine.GET(":productId", handler.GetProduct)

	testCases := []struct {
		in      string
		message string
		code    int
	}{
		{
			in:      goodProductId,
			message: resGetProduct,
			code:    http.StatusOK,
		}, {
			in:      missingProductId,
			message: resProductNotFound,
			code:    http.StatusNotFound,
		}, {
			in:      "fdsjnkfdns",
			message: resInvalid,
			code:    http.StatusBadRequest,
		}, {
			in:      internalProductId,
			message: resInternal,
			code:    http.StatusInternalServerError,
		},
	}

	for i, c := range testCases {
		resp := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodGet, "/"+c.in, nil)

		engine.ServeHTTP(resp, req)

		respBody := gin.H{}
		_ = json.Unmarshal(resp.Body.Bytes(), &respBody)

		if c.code != resp.Code {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.code, resp.Code)
		}

		if c.message != respBody["message"] {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.message, respBody["message"])
		}
	}
}

func TestHandler_UpdateProduct(t *testing.T) {
	engine := setUp()
	engine.PUT("/:productId", handler.UpdateProduct)

	testCases := []struct {
		in      string
		body    gin.H
		message string
		code    int
	}{
		{
			in:      goodProductId,
			body:    gin.H{"name": goodName},
			message: resUpdateProduct,
			code:    http.StatusOK,
		}, {
			in:      missingProductId,
			body:    gin.H{"name": goodName},
			message: resProductNotFound,
			code:    http.StatusNotFound,
		}, {
			in:      goodProductId,
			body:    gin.H{"name": ""},
			message: resInvalid,
			code:    http.StatusBadRequest,
		}, {
			in:      goodProductId,
			body:    gin.H{"name": duplicateName},
			message: resDuplicate,
			code:    http.StatusConflict,
		}, {
			in:      internalProductId,
			body:    gin.H{"name": goodProductId},
			message: resInternal,
			code:    http.StatusInternalServerError,
		}, {
			in:      "dnskjfdn",
			body:    gin.H{"name": goodName},
			message: resInvalid,
			code:    http.StatusBadRequest,
		},
	}

	for i, c := range testCases {
		resp := httptest.NewRecorder()

		body, _ := json.Marshal(c.body)
		req, _ := http.NewRequest(http.MethodPut, "/"+c.in, bytes.NewReader(body))

		engine.ServeHTTP(resp, req)

		respBody := gin.H{}
		_ = json.Unmarshal(resp.Body.Bytes(), &respBody)

		if c.code != resp.Code {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.code, resp.Code)
		}

		if c.message != respBody["message"] {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.message, respBody["message"])
		}
	}
}

func TestHandler_DeleteProduct(t *testing.T) {
	engine := setUp()
	engine.DELETE("/:productId", handler.DeleteProduct)

	testCases := [] struct {
		in      string
		message string
		code    int
	}{
		{
			in:      goodProductId,
			message: resDeleteProduct,
			code:    http.StatusOK,
		}, {
			in:      missingProductId,
			message: resProductNotFound,
			code:    http.StatusNotFound,
		}, {
			in:      internalProductId,
			message: resInternal,
			code:    http.StatusInternalServerError,
		}, {
			in:      "fefewf",
			message: resInvalid,
			code:    http.StatusBadRequest,
		},
	}

	for i, c := range testCases {
		resp := httptest.NewRecorder()
		req, _ := http.NewRequest(http.MethodDelete, "/"+c.in, nil)

		engine.ServeHTTP(resp, req)

		respBody := gin.H{}
		_ = json.Unmarshal(resp.Body.Bytes(), &respBody)

		if c.code != resp.Code {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.code, resp.Code)
		}

		if c.message != respBody["message"] {
			log.Fatalf("TestCase Number[%d]: expected [%v], got = [%v]", i, c.message, respBody["message"])
		}
	}
}
