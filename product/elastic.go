package product

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"io"
	"io/ioutil"
)

// Elastic Repo implementation is incomplete and is not used for the project

const (
	index = "product"
)

// Struct that interacts with ElasticSearch
// Implements the Repo interface
type Elastic struct {
	client *elasticsearch.Client
}

func (e *Elastic) AddProduct(entity *Entity, ctx context.Context) error {
	// First check if Name is duplicate
	query := mapping{
		"query": mapping{
			"bool": mapping{
				"must": []mapping{
					{"match": mapping{"userId": entity.UserId}},
					{"match": mapping{"name": entity.Name}},
				},
			},
		},
	}

	body, err := json.Marshal(query)
	if err != nil {
		return err
	}

	res, err := e.client.Count(
		e.client.Count.WithIndex(index),
		e.client.Count.WithBody(bytes.NewReader(body)),
		e.client.Count.WithFilterPath("count"),
		e.client.Count.WithContext(ctx),
	)
	if err != nil {
		return err
	}

	var resBody mapping
	if err := json.NewDecoder(res.Body).Decode(resBody); err != nil {
		return err
	}

	count, ok := resBody["count"].(int)
	if !ok {
		return errors.New("fail type assertion")
	}

	if count > 0 {
		return errDuplicate
	}

	// Create new document
	query = mapping{
		"userId": entity.UserId,
		"name":   entity.Name,
		"tags":   entity.Tags,
		"price":  entity.Price,
		"desc":   entity.Desc,
	}

	body, err = json.Marshal(query)
	if err != nil {
		return err
	}

	req := esapi.CreateRequest{
		Index:      index,
		Body:       bytes.NewReader(body),
		DocumentID: entity.ProductId,
		Refresh:    "true",
	}

	res, err = req.Do(ctx, e.client)
	if err != nil {
		return err
	}

	if res.StatusCode == 409 {
		return errDuplicate
	}

	return nil
}

func (e *Elastic) ListProduct(userId string, ctx context.Context) ([]Entity, error) {
	query := mapping{
		"query": mapping{
			"match": mapping{"userId": userId},
		},
	}

	body, err := json.Marshal(query)
	if err != nil {
		return nil, err
	}

	res, err := e.client.Search(
		e.client.Search.WithIndex(index),
		e.client.Search.WithBody(bytes.NewReader(body)),
		e.client.Search.WithFilterPath("hits.hits._id", "hits.hits._source"),
		e.client.Search.WithContext(ctx),
	)
	if err != nil {
		return nil, err
	}

	resBody, err := responseToStruct(res.Body)
	if err != nil {
		return nil, err
	}

	entities := make([]Entity, len(resBody.Hits.Hits))
	for i, c := range resBody.Hits.Hits {
		entities[i].ProductId = c.ProductID
		entities[i].Name = c.Source.Name
		entities[i].Tags = c.Source.Tags
		entities[i].Price = c.Source.Price
		entities[i].Desc = c.Source.Desc
	}

	return entities, nil
}

func (e *Elastic) GetProduct(entity *Entity, ctx context.Context) error {
	query := mapping{
		"query": mapping{
			"bool": mapping{
				"must": []mapping{
					{"match": mapping{"userId": entity.UserId}},
					{"match": mapping{"_id": entity.ProductId}},
				},
			},
		},
	}

	body, err := json.Marshal(query)
	if err != nil {
		return err
	}

	res, err := e.client.Search(
		e.client.Search.WithIndex(index),
		e.client.Search.WithBody(bytes.NewReader(body)),
		e.client.Search.WithFilterPath("hits.hits._id", "hits.hits._source"),
		e.client.Search.WithContext(ctx),
	)
	if err != nil {
		return err
	}

	resBody, err := responseToStruct(res.Body)
	if err != nil {
		return err
	}

	entity.Name = resBody.Hits.Hits[0].Source.Name
	entity.Tags = resBody.Hits.Hits[0].Source.Tags
	entity.Price = resBody.Hits.Hits[0].Source.Price
	entity.Desc = resBody.Hits.Hits[0].Source.Desc

	return nil
}

func (e *Elastic) SearchProduct(entity *Entity, ctx context.Context) ([]Entity, error) {
	return []Entity{}, nil
}

func (e *Elastic) UpdateProduct(entity *Entity, ctx context.Context) error {
	// Check if Product with new name already exist
	query := mapping{
		"query": mapping{
			"bool": mapping{
				"must": []mapping{
					{"match": mapping{"userId": entity.UserId}},
					{"match": mapping{"name": entity.Name}},
				},
				"must_not": mapping{"match": mapping{"_id": entity.ProductId}},
			},
		},
	}

	body, err := json.Marshal(query)
	if err != nil {
		return err
	}

	res, err := e.client.Count(
		e.client.Count.WithIndex(index),
		e.client.Count.WithBody(bytes.NewReader(body)),
		e.client.Count.WithFilterPath("count"),
		e.client.Count.WithContext(ctx),
	)
	if err != nil {
		return err
	}

	var resBody mapping
	if err := json.NewDecoder(res.Body).Decode(resBody); err != nil {
		return err
	}

	count, ok := resBody["count"].(int)
	if !ok {
		return errors.New("fail type assertion")
	}

	if count > 0 {
		return errDuplicate
	}

	// Update document
	// Build the update string
	inline := fmt.Sprintf("ctx._source.name=%s; ctx.source_price=%f; ctx._source.desc=%s; ctx._source.tag=[",
		entity.Name, entity.Price, entity.Desc)

	for i := range entity.Tags {
		inline += fmt.Sprintf("'%s'", entity.Tags[i])

		if i < len(entity.Tags)-1 {
			inline += ","
		}
	}

	inline += "];"

	query = mapping{
		"query": mapping{
			"bool": mapping{
				"must": []mapping{
					{"match": mapping{"userId": entity.UserId}},
					{"match": mapping{"_id": entity.ProductId}},
				},
			},
		},
		"script": mapping{
			"inline": inline,
			"lang":   "painless",
		},
	}

	body, err = json.Marshal(query)
	if err != nil {
		return err
	}

	res, err = e.client.UpdateByQuery(
		[]string{index},
		e.client.UpdateByQuery.WithBody(bytes.NewReader(body)),
		e.client.UpdateByQuery.WithFilterPath("updated"),
		e.client.UpdateByQuery.WithContext(ctx),
	)
	if err != nil {
		return err
	}

	if err := json.NewDecoder(res.Body).Decode(resBody); err != nil {
		return err
	}

	if res.StatusCode != 200 {
		return errors.New("internal error")
	}

	updated, ok := resBody["updated"].(int)
	if !ok {
		return errors.New("fail type assertion")
	}

	if updated != 1 {
		return errors.New("internal error")
	}

	return nil
}

func (e *Elastic) DeleteProduct(userId string, productId string, ctx context.Context) error {
	query := mapping{
		"query": mapping{
			"bool": mapping{
				"must": []mapping{
					{"match": mapping{"userId": userId}},
					{"match": mapping{"_id": productId}},
				},
			},
		},
	}

	body, err := json.Marshal(query)
	if err != nil {
		return err
	}

	res, err := e.client.DeleteByQuery(
		[]string{index},
		bytes.NewReader(body),
		e.client.DeleteByQuery.WithContext(ctx),
	)
	if err != nil {
		return err
	}

	temp, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	var resBody mapping
	err = json.Unmarshal(temp, &resBody)

	if res.StatusCode != 200 {
		return errors.New("internal error")
	}

	deleted, ok := resBody["deleted"].(int)
	if !ok {
		return errors.New("fail type assertion")
	}

	if deleted < 1 {
		return errNotFound
	}

	return nil
}

// Structs response from ElasticSearch
type ElasticResponse struct {
	Hits struct {
		Hits []struct {
			ProductID string `json:"_id"`
			Source    struct {
				UserID string   `json:"userId"`
				Name   string   `json:"name"`
				Tags   []string `json:"tags"`
				Price  float64  `json:"price"`
				Desc   string   `json:"desc"`
			} `json:"_source,omitempty"`
		} `json:"hits"`
	} `json:"hits"`
}

func responseToStruct(reader io.Reader) (*ElasticResponse, error) {
	temp, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	res := ElasticResponse{}
	if err = json.Unmarshal(temp, &res); err != nil {
		return nil, err
	}

	return &res, nil
}
