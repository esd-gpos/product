package main

import (
	"context"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/esd-gpos/auth/middleware"
	"gitlab.com/esd-gpos/product/product"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	// Initialize Gin
	engine := gin.New()

	corsConfig := cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: false,
		MaxAge:           12 * time.Hour,
	}

	engine.Use(cors.New(corsConfig))

	// Create authentication middleware
	audience := os.Getenv("AUDIENCE")
	issuer := os.Getenv("ISSUER")
	publicKey := os.Getenv("PUBLIC_KEY")

	if audience == "" || issuer == "" || publicKey == "" {
		log.Fatalf("error: missing env variable\nAUDIENCE: %s\nISSUER: %s\nPUBLIC_KEY: %s\n",
			audience, issuer, publicKey)
	}

	info := &middleware.Information{
		Audience:  audience,
		Issuer:    issuer,
		PublicKey: publicKey,
	}

	authMiddleware, err := middleware.NewMiddleware(info)
	if err != nil {
		log.Fatal("could not create authMiddleware")
	}

	// Create MongoRepo
	database := os.Getenv("PRODUCT_MONGO_DATABASE")
	collection := os.Getenv("PRODUCT_MONGO_COLLECTION")
	mongoUri := os.Getenv("PRODUCT_MONGO_URI")
	if mongoUri == "" || collection == "" || database == "" {
		log.Fatalf("error missing env variable\nPRODUCT_MONGO_URI: %s\nPRODUCT_MONGO_DATABASE: %s\n"+
			"PRODUCT_MONGO_COLLECTION: %s\n", mongoUri, database, collection)
	}

	mongoClient, err := mongo.NewClient(options.Client().ApplyURI(mongoUri))
	if err != nil {
		log.Fatalf("could not create mongo client: %s\n", err)
	}

	if err = mongoClient.Connect(context.Background()); err != nil {
		log.Fatalf("could not start connection: %s\n", err)
	}

	mongoCollection := mongoClient.Database(database).Collection(collection)
	mongoRepo := product.NewMongoRepo(mongoCollection)

	// Create Product service Handler and register routes
	handler := product.NewHandler(mongoRepo)
	product.Setup(engine, authMiddleware, handler)

	// Readiness Endpoint
	engine.GET("/ping", func(ctx *gin.Context) {
		if err := mongoClient.Ping(ctx, nil); err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": err})
			return
		}

		ctx.JSON(http.StatusOK, "Ready to Serve")
	})

	// Start server
	log.Fatal(engine.Run("0.0.0.0:7400"))
}
