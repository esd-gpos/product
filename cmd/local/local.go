package main

// Example Product Service server
// Pass in plain uuid instead of actual Bearer token

import (
	"context"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/esd-gpos/product/product"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"strings"
)

const userId = "88fadf90-a7bd-4578-8f0b-976639f3b545"

func main() {
	// Init mongo
	mongoClient, err := mongo.NewClient(options.Client().ApplyURI("mongodb://username:password@localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}

	err = mongoClient.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	collection := mongoClient.Database("testing").Collection("handler")
	mongoRepo := product.NewMongoRepo(collection)

	// Init server
	productHandler := product.NewHandler(mongoRepo)

	engine := gin.New()
	engine.Use(func(ctx *gin.Context) {
		auth := ctx.GetHeader("Authorization")
		if auth == "" {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		token := strings.Split(auth, " ")
		if len(token) != 2 {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		if token[0] != "Bearer" {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		if _, err := uuid.Parse(token[1]); err != nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		ctx.Set("userId", token[1])
	})

	product.Setup(engine, productHandler)

	log.Fatal(engine.Run("0.0.0.0:7400"))
}
