package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func main() {

	// Create mongo client
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://username:password@localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}

	// Connect to mongo
	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	collection := client.Database("testing").Collection("example")

	// Update document
	update, err := collection.UpdateOne(context.Background(), bson.M{"userId": "1290", "_id": 1}, bson.M{
		"$set": bson.M{
			"_id":    "1",
			"userId": "1290",
			"name":   "Dell GS5",
			"tags":   []string{"tech"},
			"price":  500,
			"desc":   "Super Brand new Lenovo laptop",
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println(update.ModifiedCount)
}
