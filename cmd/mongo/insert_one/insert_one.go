package main

// Example usage of mongo package
// Shows usage of InsertOne function
// Requires index to be set

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func main() {

	// Create mongo client
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://username:password@localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}

	// Connect to mongo
	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	collection := client.Database("testing").Collection("example")

	// Insert document
	_, err = collection.InsertOne(context.Background(), bson.M{
		"_id":    "1",
		"userId": "1290",
		"name":   "Lenovo A10",
		"tags":   []string{"tech"},
		"price":  300,
		"desc":   "Brand new Lenovo laptop",
	})
	if err != nil {
		writeError := err.(mongo.WriteException)
		log.Fatal(writeError.WriteErrors[0].Code)
	}
}
