package main

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func main() {

	// Create mongo client
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://username:password@localhost:27017"))
	if err != nil {
		log.Fatal(err)
	}

	// Connect to mongo
	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	collection := client.Database("testing").Collection("example")

	// Find and replace document
	single := collection.FindOneAndReplace(context.Background(), bson.M{"userId": "1290", "_id": "1"}, bson.M{
		"userId": "1290",
		"name":   "Dell GS5",
		"tags":   []string{"tech"},
		"price":  250,
		"desc":   "Old Lenovo laptop",
	})

	if err, ok := single.Err().(mongo.CommandError); ok {
		if err.Code == 11000 {
			log.Println("duplicate")
		}

		log.Println(err)
	}
}
